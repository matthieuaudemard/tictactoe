package taridev.jva250.tictactoe.gui;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class TicTacToeGui extends JFrame {

    /**
     * Panel de jeu
     */
    private TicTacToePanel gamePanel = new TicTacToePanel();

    public TicTacToeGui() {
        super("TicTacToe");
        setJMenuBar(createMenuBar());
        add(gamePanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(300, 350);
        setResizable(false);
    }

    /**
     * Crée la barre de menu du jeu
     * @return la JMenuBar créée
     */
    private JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu gameMenu = new JMenu("Game");
        JMenuItem newGameMenuItem = new JMenuItem("New...");
        JMenuItem quitMenuItem = new JMenuItem("Quit");

        // Association à l'action à effectuer lors que l'item "New..." est selectionné
        newGameMenuItem.addActionListener(e -> gamePanel.init());
        // On associe "New..." à la combinaison de touche [Alt+ N]
        newGameMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.ALT_MASK));
        gameMenu.add(newGameMenuItem); // On ajoute l'item "New.." au menu "Game"
        gameMenu.addSeparator();       // Ajout d'un séparateur dans le menu par soucis de présentation

        // Association à l'action à effectuer lors que l'item "Quit" est selectionné (on quitte le programme)
        quitMenuItem.addActionListener(e -> System.exit(0));
        // On associe "Quit" à la combinaison de touche [Alt+ Q]
        quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.ALT_MASK));
        gameMenu.add(quitMenuItem);    // on ajoute l'item "Quit" au menu "Game"
        menuBar.add(gameMenu);         // On ajoute le menu "Game" à la MenuBar
        return menuBar;
    }
}
