package taridev.jva250.tictactoe.gui;

import javax.swing.*;
import java.awt.*;

class TicTacToeMessage {

    private static final ImageIcon LOGO_IMAGE = new ImageIcon("rsc/tictactoe_icon.png");

    private TicTacToeMessage() { }

    static int getGameEndedMessage(Component parent, String message) {
        return JOptionPane.showOptionDialog(parent,
                message,
                "Partie terminée",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                TicTacToeMessage.LOGO_IMAGE,
                null,
                null);
    }
}
