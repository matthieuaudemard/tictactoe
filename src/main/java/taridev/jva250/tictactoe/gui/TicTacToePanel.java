package taridev.jva250.tictactoe.gui;

import taridev.jva250.tictactoe.engine.TicTacToeGameStateEnum;
import taridev.jva250.tictactoe.engine.TicTacToeGrid;
import taridev.jva250.tictactoe.engine.TicTacToeGame;
import taridev.jva250.tictactoe.engine.TicTacToeSeedEnum;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class TicTacToePanel extends JPanel {

    /**
     * Le jeu
     */
    private transient TicTacToeGame game = new TicTacToeGame();

    private static final String GRID_IMAGE_PATH = "rsc/tictactoe_grid.png";
    private static final String CROSS_IMAGE_PATH = "rsc/tictactoe_cross.png";
    private static final String NOUGHT_IMAGE_PATH = "rsc/tictactoe_nought.png";

    private static final BufferedImage GRID_IMAGE = TicTacToeHelper.readImage(TicTacToePanel.GRID_IMAGE_PATH);
    private static final BufferedImage CROSS_IMAGE = TicTacToeHelper.readImage(TicTacToePanel.CROSS_IMAGE_PATH);
    private static final BufferedImage NOUGHT_IMAGE = TicTacToeHelper.readImage(TicTacToePanel.NOUGHT_IMAGE_PATH);

    TicTacToePanel() {
        // Ajout du listener de click sur le plateau de jeu
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Etablissement de la correspondance entre les coordonées (x, y) du click et
                // les coordonées (row, col) dans la grille
                int row = getGridCoordFromEventX(e.getX());
                int col = getGridCoordFromEventY(e.getY());

                // On place l'élément aux coordonées déterminées. Si le jeu est gagné alors won = true
                boolean played = game.play(row, col);
                repaint();

                // Affichage d'un message pour rejouer si la partie est gagnée ou s'il la partie est terminée
                if (played && game.getState() == TicTacToeGameStateEnum.WON
                        && TicTacToeMessage.getGameEndedMessage(TicTacToePanel.this,
                        game.getCurrentPlayer() +  " a gagné, voulez-vous recommencer ?") == JOptionPane.OK_OPTION
                        || game.getState() == TicTacToeGameStateEnum.ENDED
                        && TicTacToeMessage.getGameEndedMessage(TicTacToePanel.this,
                        "Match nul, voulez-vous recommencer ?") == JOptionPane.OK_OPTION) {
                    init();
                }
            }
        });
    }

    /**
     * Initialise une partie et repaint la grille
     */
    void init() {
        game.init();
        repaint();
    }

    /**
     * Permet de détermine la ligne dans la grille à partir d'une abscisse du panel
     * @param x l'abscisse du panel
     * @return la ligne dans le tableau correspondant à l'abscisse du panel
     */
    private int getGridCoordFromEventX(int x) {
        int i = 0;
        // Si l'abscisse du click est comprise entre le 1 et le 2ème 1/3 du panel
        if (x > getWidth() / 3 && x < 2 * getWidth() / 3) {
            i = 1;
        }
        // Si l'abscisse du click est comprise entre dans le 3 1/3 du panel
        else if (x > 2 * getWidth() / 3) {
            i = 2;
        }
        return i;
    }

    /**
     * Permet de détermine la colonne dans la grille à partir d'une ordonnée du panel
     * @param y l'ordonnée du panel
     * @return la colonne dans le tableau correspondant à l'ordonnée du panel
     */
    private int getGridCoordFromEventY(int y) {
        int j = 0;
        // Si l'ordonnée du click est comprise entre le 1 et le 2ème 1/3 du panel
        if (y > getHeight() / 3 && y < 2 * getHeight() / 3) {
            j = 1;
        }
        // Si l'ordonnée du click est comprise entre dans le 3 1/3 du panel
        else if (y > 2 * getHeight() / 3) {
            j = 2;
        }
        return j;
    }

    /**
     * Permet de dessiner le plateau de jeu
     * @param g le Graphics du panel
     */
    @Override
    protected void paintComponent(Graphics g) {
        // Dessin du quadrillage
        drawGrid(g);
        // Dessin des croix et cercles présents dans la grille
        for(int i = 0; i < TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j = 0; j < TicTacToeGrid.GRID_SIZE ; j++) {
                TicTacToeSeedEnum seed = game.getSeedAt(i, j);
                if (seed == TicTacToeSeedEnum.CROSS) {
                    drawCross(g, i, j);
                }
                else if (seed == TicTacToeSeedEnum.NOUGHT) {
                    drawNought(g, i, j);
                }
            }
        }
    }

    /**
     * Dessine le plateau de jeu
     * @param g le Graphics du panel
     */
    private void drawGrid(Graphics g) {
        g.drawImage(TicTacToePanel.GRID_IMAGE, 0, 0, null);
    }

    /**
     * Dessine une croix sur le plateau à l'aide des coordonées (i, j) du tableau de jeu
     * @param g le Graphics du panel
     * @param i ligne dans le tableau de jeu
     * @param j colonne dans le tableau de jeu
     */
    private void drawCross(Graphics g, int i, int j) {
            g.drawImage(TicTacToePanel.CROSS_IMAGE, getWidth() / 3 * i, 5 + getHeight() / 3 * j, null);
    }

    /**
     * Dessine un cercle sur le plateau à l'aide des coordonées (i, j) du tableau de jeu
     * @param g le Graphics du panel
     * @param i ligne dans le tableau de jeu
     * @param j colonne dans le tableau de jeu
     */
    private void drawNought(Graphics g, int i, int j) {
        g.drawImage(TicTacToePanel.NOUGHT_IMAGE, getWidth() / 3 * i, 5 + getHeight() / 3 * j, null);
    }
}
