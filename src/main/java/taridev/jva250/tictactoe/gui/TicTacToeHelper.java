package taridev.jva250.tictactoe.gui;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;

public class TicTacToeHelper {

    private TicTacToeHelper() { }

    public static BufferedImage readImage(final String path) {
        try {
            return ImageIO.read(new FileInputStream(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
