package taridev.jva250.tictactoe.engine;

/**
 * La classe TicTacToeGrid représente la grille de jeu
 */
public class TicTacToeGrid {

    /**
     * Taille du plateau de jeu
     */
    public static final int GRID_SIZE = 3;

    /**
     * Tableau de jeu
     */
    private TicTacToeSeedEnum[][] grid;

    TicTacToeGrid() {
        grid = new TicTacToeSeedEnum[GRID_SIZE][GRID_SIZE];
        for (int i=0 ; i<TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j=0 ; j<TicTacToeGrid.GRID_SIZE ; j++) {
                grid[i][j] = TicTacToeSeedEnum.EMPTY;
            }
        }
    }

    public void init() {
        for (int i=0 ; i<TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j=0 ; j<TicTacToeGrid.GRID_SIZE ; j++) {
                grid[i][j] = TicTacToeSeedEnum.EMPTY;
            }
        }
    }

    /**
     * Renvoie le contenu de la case [i][j] du tableau de jeu
     * @param i colonne du tableau
     * @param j ligne du tableau
     * @return TicTacToeSeedEnum contenu à la case [i][j]
     */
    public TicTacToeSeedEnum getSeedAt(int i, int j) {
        return grid[i][j];
    }

    /**
     * Met une valeur en paramètre à la case [i][j] du tableau de jeu
     * @param seed soit une croix, un cercle ou "vide"
     * @param i colonne du tableau
     * @param j ligne du tableau
     */
    public void setSeedAt(TicTacToeSeedEnum seed, int i, int j) {
        grid[i][j] = seed;
    }
}
