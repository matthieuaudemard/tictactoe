package taridev.jva250.tictactoe.engine;

/**
 * L'énumeration TicTacToeSeedEnum représente les différents état que l'on peut trouver dans @see TicTacToeGrid.
 */
public enum TicTacToeSeedEnum {

    /**
     * Etat associé aux cases vides
     */
    EMPTY (" "), // Correspond à une case vide

    /**
     * Etat associé aux cases contenant une croix
     */
    CROSS ("\u00D7"), // Correspond à une croix

    /**
     * Etat associé aux cases contenant un cercle
     */
    NOUGHT ("O");

    private String litteral;

    TicTacToeSeedEnum(String litteral) {
        this.litteral = litteral;
    }

    @Override
    public String toString() {
        return litteral;
    }

}
