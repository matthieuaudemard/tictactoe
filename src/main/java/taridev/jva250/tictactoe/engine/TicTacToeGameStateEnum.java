package taridev.jva250.tictactoe.engine;

/**
 * Cette énumération décrit les différents états possibles
 * pris par un TicTacToeGame
 */
public enum TicTacToeGameStateEnum {
    /**
     * La partie est en cours
     */
    PLAYING,

    /**
     * La partie est terminée sans aucun vainqueur
     */
    ENDED,

    /**
     * La partie est gagnée
     */
    WON
}
