package taridev.jva250.tictactoe.engine;

/**
 * La classe TicTacToeGame représente le controleur du jeu.
 * Elle contient une gille de jeu (TicTacToeGrid) qu'elle va controler
 * ainsi que les informations nécessaires au jeu comme l'état de la partie (en cours ou fini),
 * le joueur en cours ...
 */
public class TicTacToeGame {

    public static final TicTacToeSeedEnum INITIAL_PLAYER = TicTacToeSeedEnum.CROSS;

    /**
     * La grille de jeu
     */
    private TicTacToeGrid grid;

    /**
     * Etat de la partie
     */
    private TicTacToeGameStateEnum state;

    /**
     * Le joueur courant (Croix, Cercle)
     */
    private TicTacToeSeedEnum currentPlayer;

    /**
     * Le nombre d'action possibles restantes (permet de savoir s'il y a game-over)
     */
    private int playerActionsLeft = TicTacToeGrid.GRID_SIZE * TicTacToeGrid.GRID_SIZE;

    /**
     * construit une nouvelle grille de jeu et initialise les états.
     */
    public TicTacToeGame() {
        grid = new TicTacToeGrid();
        init();
    }

    public TicTacToeGrid getGrid() {
        return grid;
    }

    public TicTacToeGameStateEnum getState() {
        return state;
    }

    /**
     * Permet de connaître le contenu de la case [i][j] dans la grille
     * @param i la ligne de la grille
     * @param j la colonne de la grille
     * @return le signe contenu à la case [i][j] de la grille (EMPTY, CROSS ou NOUGHT)
     */
    public TicTacToeSeedEnum getSeedAt(int i, int j) {
        return grid.getSeedAt(i, j);
    }

    /**
     * Permet de connaitre le joueur actif
     * @return le joueur actif
     */
    public TicTacToeSeedEnum getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Vide la grille, change l'état à PLAYIN, le joueur courant à CROSS
     * et réinitialise le nombre d'actions restantes
     */
    public void init() {
        grid.init();
        state = TicTacToeGameStateEnum.PLAYING;
        currentPlayer = TicTacToeGame.INITIAL_PLAYER;
        playerActionsLeft = TicTacToeGrid.GRID_SIZE * TicTacToeGrid.GRID_SIZE;
    }

    /**
     * Essaye de faire jouer le joueur courant à la case [i][j]. Si cette case est vide alors on y place le signe
     * correspondant du joueur courant et on change le joueur courant.
     * @param i ligne du tableau de jeu
     * @param j colonne du tableau de jeu
     * @return true si la partie est gagnée, false sinon
     */
    public boolean play(int i, int j) {
        try {
            if (state == TicTacToeGameStateEnum.PLAYING && grid.getSeedAt(i, j) == TicTacToeSeedEnum.EMPTY) {
                grid.setSeedAt(currentPlayer, i, j);
                playerActionsLeft --;
                if (hasWon(currentPlayer, i, j)) {
                    state = TicTacToeGameStateEnum.WON;
                } else if (!gameEnded()) {
                    currentPlayer = (currentPlayer == TicTacToeSeedEnum.CROSS) ? TicTacToeSeedEnum.NOUGHT : TicTacToeSeedEnum.CROSS;
                } else {
                    state = TicTacToeGameStateEnum.ENDED;
                }
                return true;
            }
            return false;
        }

        catch (ArrayIndexOutOfBoundsException e) {
            return false;
        }
    }

    /**
     * Permet de savoir si le joueur associé au symbole en paramêtre a gagné.
     * @param seed Symbole du joueur
     * @param i ligne du tableau de jeu
     * @param j colonne du tableau de jeu
     * @return true si il y dans la grille 3 seed symboles alignes, sinon false
     */
    private boolean hasWon(TicTacToeSeedEnum seed, int i, int j) {
        return (grid.getSeedAt(i, 0) == seed      // On vérifie 3 en ligne
                && grid.getSeedAt(i, 1) == seed
                && grid.getSeedAt(i, 2) == seed
                || grid.getSeedAt(0, j) == seed   // On vérifie 3 en colonne
                && grid.getSeedAt(1, j) == seed
                && grid.getSeedAt(2, j) == seed
                || i == j                           // On vérifie 3 en diagonale
                && grid.getSeedAt(0, 0) == seed
                && grid.getSeedAt(1, 1) == seed
                && grid.getSeedAt(2, 2) == seed
                || i + j == 2                       // On vérifie 3 en diagonale opposée
                && grid.getSeedAt(0, 2) == seed
                && grid.getSeedAt(1, 1) == seed
                && grid.getSeedAt(2, 0) == seed);
    }

    private boolean gameEnded() {
        return playerActionsLeft <= 0;
    }
}
