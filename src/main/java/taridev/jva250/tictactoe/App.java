package taridev.jva250.tictactoe;

import taridev.jva250.tictactoe.gui.TicTacToeGui;

import javax.swing.*;

public class App extends JFrame {

    public static void main(String[] args) {
        new TicTacToeGui().setVisible(true);
    }

}
