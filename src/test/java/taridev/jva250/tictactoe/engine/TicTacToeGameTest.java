package taridev.jva250.tictactoe.engine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TicTacToeGameTest {

    private TicTacToeGame game;

    @Before
    public void setUp() {
        game = new TicTacToeGame();
    }

    @Test
    public void setAndGetGrid() {
    }

    @Test
    public void getState() {
        Assert.assertEquals(TicTacToeGameStateEnum.PLAYING, game.getState()); // Etat initial
    }

    @Test
    public void init() {
        game.init();
        Assert.assertEquals(TicTacToeGame.INITIAL_PLAYER, game.getCurrentPlayer());
        Assert.assertEquals(TicTacToeGameStateEnum.PLAYING, game.getState()); // Etat initial
        for (int i=0 ; i<TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j=0 ; j<TicTacToeGrid.GRID_SIZE ; j++) {
                Assert.assertEquals(TicTacToeSeedEnum.EMPTY, game.getSeedAt(i, j));
            }
        }
    }

    @Test
    public void play() {
    }
}