package taridev.jva250.tictactoe.engine;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class TicTacToeGridTest {

    private TicTacToeGrid grid;

    public static TicTacToeGrid createRandomGrid() {
        TicTacToeSeedEnum[] ticTacToeSeedEnums = {
                TicTacToeSeedEnum.EMPTY,
                TicTacToeSeedEnum.CROSS,
                TicTacToeSeedEnum.NOUGHT,
        };
        TicTacToeGrid result = new TicTacToeGrid();
        Random random = new Random();
        // Remplissage de la grille avec des valeurs aléatoires de TicTacToeSeedEnum
        for (int i=0 ; i<TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j=0 ; j<TicTacToeGrid.GRID_SIZE ; j++) {
                result.setSeedAt(ticTacToeSeedEnums[random.nextInt(ticTacToeSeedEnums.length)], i, j);
            }
        }
        return result;
    }

    @Before
    public void setUp() {
        grid = TicTacToeGridTest.createRandomGrid();
    }

    @Test
    public void setAndGetIndexBoundsSeedAt() {
        int i = (int) (TicTacToeGrid.GRID_SIZE * Math.random());
        int j = (int) (TicTacToeGrid.GRID_SIZE * Math.random());
        int seed = (int) (2 * Math.random());
        switch (seed) {
            case 0:
                grid.setSeedAt(TicTacToeSeedEnum.CROSS, i, j);
                Assert.assertEquals(TicTacToeSeedEnum.CROSS, grid.getSeedAt(i, j));
                break;
            case 1:
                grid.setSeedAt(TicTacToeSeedEnum.NOUGHT, i, j);
                Assert.assertEquals(TicTacToeSeedEnum.NOUGHT, grid.getSeedAt(i, j));
                break;
            default:
                grid.setSeedAt(TicTacToeSeedEnum.EMPTY, i, j);
                Assert.assertEquals(TicTacToeSeedEnum.EMPTY, grid.getSeedAt(i, j));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setSeedAtLineIndexOutOfBounds() {
        grid.setSeedAt(TicTacToeSeedEnum.CROSS,
                TicTacToeGrid.GRID_SIZE,
                (int) (TicTacToeGrid.GRID_SIZE * Math.random()));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void setSeedAtColumnIndexOutOfBounds() {
        grid.setSeedAt(TicTacToeSeedEnum.CROSS,
                (int) (TicTacToeGrid.GRID_SIZE * Math.random()),
                TicTacToeGrid.GRID_SIZE);
    }

    @Test
    public void init() {
        grid.init();
        for (int i=0 ; i<TicTacToeGrid.GRID_SIZE ; i++) {
            for (int j=0 ; j<TicTacToeGrid.GRID_SIZE ; j++) {
                Assert.assertEquals(TicTacToeSeedEnum.EMPTY, grid.getSeedAt(i, j));
            }
        }
    }
}